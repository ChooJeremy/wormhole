from django.db import models

import datetime
import hashlib
import time

from django.utils import timezone

# Create your models here.
class Lecture(models.Model):
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=5, blank=True)
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.url == '':
            #Generate URL off hash
            hash = hashlib.sha1()
            hash.update(str(time.time()))
            self.url = hash.hexdigest()[:5]
        super(Lecture, self).save(*args, **kwargs) # Call the "real" save() method.

class AudienceLogin(models.Model):
    username = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    lecture = models.ForeignKey(Lecture)
    def __unicode__(self):  # Python 3: def __str__(self):
        return "name: " + self.username + "    email: " + self.email

class Question(models.Model):
    question_text = models.TextField()
    lecture = models.ForeignKey(Lecture)
    answer_text = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now=True)
    questioner = models.ForeignKey(AudienceLogin, related_name="questions_asked")
    answered = models.BooleanField()
    voters = models.ManyToManyField(AudienceLogin, related_name="questions_voted", null=True, blank=True)
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.question_text