from django.shortcuts import render
from django.template import loader, Context, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import *
from django.contrib.auth.models import User
from django.db.models import Count
from wormholeapp.models import *
from django.core.exceptions import ObjectDoesNotExist

def getUser(email, lecture):
    if email is None:
        print "Not logged in user attempted access to lecture" + str(lecture.id)
        return None
    elif AudienceLogin.objects.all().filter(email=email).exists() == False:
        print "Invalid user attempted access to lecture " + str(lecture.id)
        return None
    elif AudienceLogin.objects.all().filter(email=email).filter(lecture=lecture).exists() == False:
        print "User with email " + str(email) + " does not have access to lecture " + str(lecture.id)
        return None
    else:
        person = AudienceLogin.objects.all().filter(email=email).filter(lecture=lecture)[0]
        return person

def getLecture(hash):
    try:
        lecture = Lecture.objects.get(url=hash)
        return lecture
    except Lecture.DoesNotExist:
        return None

# Create your views here.
def lecture(request, lectureHash):
    # Check if the lecture is valid
    lecture = getLecture(lectureHash)
    if lecture is None:
        return HttpResponseRedirect("/")

    # Check if the user log in is valid
    email = request.COOKIES.get('login', None)
    theUser = getUser(email, lecture)
    if theUser is None:
        # Test for admin
        if request.user.is_authenticated() == False:
            return HttpResponseRedirect('/')

    #now the user is either an allowed user or an admin user,

    # If this user is entering a quesiton, add it in
    if "question" in request.POST:
        processQuestion(request, lecture, theUser, request.REQUEST.__getitem__('question'))

    questions = Question.objects.all().filter(lecture=lecture).annotate(total_votes=Count('voters')).order_by('-total_votes')

    #Admin, do admin-based stuff:
    if request.user.is_authenticated():
        # check for audience view
        if int(request.GET.get("audience", 0)) == 1:
            t = loader.get_template('AdminAudienceView.html')
        else:
            t = loader.get_template('AdminQuestionList.html')
    #User
    else:
        for aQuestion in questions:
            if theUser in aQuestion.voters.all():
                aQuestion.voted = True
            else:
                aQuestion.voted = False
        t = loader.get_template('QuestionList.html')

    c = RequestContext(request, {'questions': questions, 'lectureHash': lectureHash, 'lecture': lecture})
    return HttpResponse(t.render(c))

def userLogin(request):
    # Delete their cookie as well, if any.
    t = loader.get_template('Login.html')
    c = RequestContext(request)
    answer = HttpResponse(t.render(c))
    answer.delete_cookie('login')
    return answer

def processLogin(request):
    if "email" in request.POST and "name" in request.POST and "lecture-hash" in request.POST:
        logins = request.REQUEST
        email = logins.__getitem__("email")
        name = logins.__getitem__("name")
        # Attempt to get the Lecture
        try:
            lectureHash = logins.__getitem__("lecture-hash")
            print str(name) + " is attempting to enter lecture " + str(lectureHash)
            lecture = getLecture(lectureHash)
            if lecture is None:
                raise Lecture.DoesNotExist
            print "Lecture found."
            # Check if entry already exists
            entry = AudienceLogin.objects.all().filter(email=email).filter(lecture=lecture)
            if entry.exists():
                al = entry.get()
                if al.username != name:
                    print "Login done with different preferred name"
                    return HttpResponseRedirect("/")
            else:        
                al = AudienceLogin(username=name, email=email, lecture=lecture)
                al.save()
            redirect = HttpResponseRedirect(str(lectureHash))
            redirect.set_cookie('login', email)
            return redirect
        except Lecture.DoesNotExist:
            print "No such lecture."
            return HttpResponseRedirect("/")
    elif "lecture-hash" in request.POST and "username" in request.POST and "password" in request.POST:
        logins = request.REQUEST
        username = logins.__getitem__("username")
        password = logins.__getitem__("password")
        lectureHash = logins.__getitem__("lecture-hash")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(str(lectureHash))
        else:
            return HttpResponseRedirect("adminlogin")
    else:
        return HttpResponseRedirect("/")

def processQuestion(request, lecture, user, question_text):
    theQuestion = Question(question_text=question_text,lecture=lecture,answer_text='',questioner=user,answered=False)
    theQuestion.save()

def doUpvote(request):
    email = request.COOKIES.get('login', None)
    try:
        if "lectureHash" in request.POST and "questionID" in request.POST:
            lectureHash = request.REQUEST.__getitem__("lectureHash")
            questionID = request.REQUEST.__getitem__("questionID")
            lecture = getLecture(lectureHash)
            question = Question.objects.get(pk=questionID)
            person = getUser(email, lecture)
            # Checks that must pass
            # The person attempting to vote must be real
            if person == None:
                raise ObjectDoesNotExist
            # The question must be asked in the lecture passed in
            if question.lecture != lecture:
                raise ObjectDoesNotExist
            # The person must not have upvoted that thing
            if person in question.voters.all():
                raise ObjectDoesNotExist
            # Everything passes
            question.voters.add(person)
            return HttpResponse("OK")
        else:
            return HttpResponse("NO")
    except ObjectDoesNotExist:
        print "Attempt to upvote failed: a single check failed"
        print "Email: " + str(email)
        print request.POST
        return HttpResponse("NO")

def doDownvote(request):
    email = request.COOKIES.get('login', None)
    try:
        if "lectureHash" in request.POST and "questionID" in request.POST:
            lectureHash = request.REQUEST.__getitem__("lectureHash")
            questionID = request.REQUEST.__getitem__("questionID")
            lecture = getLecture(lectureHash)
            question = Question.objects.get(pk=questionID)
            person = getUser(email, lecture)
            # Checks that must pass
            # The person attempting to vote must be real
            if person == None:
                raise ObjectDoesNotExist
            # The question must be asked in the lecture passed in
            if question.lecture != lecture:
                raise ObjectDoesNotExist
            # The person must have upvoted that thing
            if person in question.voters.all() == False:
                raise ObjectDoesNotExist
            # Everything passes
            question.voters.remove(person)
            return HttpResponse("OK")
        else:
            return HttpResponse("NO")
    except ObjectDoesNotExist:
        print "Attempt to downvote failed: a single check failed"
        print "Email: " + str(email)
        print request.POST
        return HttpResponse("NO")

def questionUpdate(request):
    email = request.COOKIES.get('login', None)
    try:
        if "lectureHash" in request.POST:
            lectureHash = request.REQUEST.__getitem__("lectureHash")
            lecture = getLecture(lectureHash)

            t = None

            #Verification check, load the template appropriately
            if request.user.is_authenticated():
                audience = request.POST.get("audience", 0)
                if int(audience) == 1:
                    t = loader.get_template('AudienceQuestionTable.html')
                else:
                    t = loader.get_template('AdminQuestionTable.html')
            else:
                if email is not None:
                    person = getUser(email, lecture)
                    # The person attempting to vote must be real
                    if person == None:
                        raise ObjectDoesNotExist
                    t = loader.get_template('QuestionTable.html')
                else:
                    raise ObjectDoesNotExist

            questions = Question.objects.all().filter(lecture=lecture).annotate(total_votes=Count('voters')).order_by('-total_votes')
            #Only need to do votes if it's an AudienceLogin
            if request.user.is_authenticated() == False:
                if email is not None:
                    for aQuestion in questions:
                        if person in aQuestion.voters.all():
                            aQuestion.voted = True
                        else:
                            aQuestion.voted = False

            c = RequestContext(request, {"questions": questions})
            return HttpResponse(t.render(c))
        else:
            return HttpResponse("NO")
    except ObjectDoesNotExist:
        print "Attempt to getQuestions failed: a single check failed"
        print "Email: " + str(email)
        print request.POST
        return HttpResponse("NO")

def adminLogin(request):
    # Delete their cookie as well, if any.
    logout(request)
    t = loader.get_template('AdminLogin.html')
    c = RequestContext(request)
    return HttpResponse(t.render(c))

def deleteQuestion(request):
    try:
        if "lectureHash" in request.POST and "questionID" in request.POST:
            lectureHash = request.REQUEST.__getitem__("lectureHash")
            lecture = getLecture(lectureHash)
            questionID = request.REQUEST.__getitem__("questionID")

            #Verification check, load the template appropriately
            if request.user.is_authenticated() is False:
                raise ObjectDoesNotExist

            if lecture is None:
                raise ObjectDoesNotExist

            item = Question.objects.all().get(pk=questionID)
            item.delete()
            return HttpResponse("OK")
        else:
            raise ObjectDoesNotExist
    except ObjectDoesNotExist:
        print "Attempt to deleteQuestion failed: a single check failed"
        print request.POST
        return HttpResponse("NO")

def answerQuestion(request):
    try:
        if "lectureHash" in request.POST and "questionID" in request.POST:
            lectureHash = request.REQUEST.__getitem__("lectureHash")
            lecture = getLecture(lectureHash)
            questionID = request.REQUEST.__getitem__("questionID")

            #Verification check, load the template appropriately
            if request.user.is_authenticated() is False:
                raise ObjectDoesNotExist

            if lecture is None:
                raise ObjectDoesNotExist

            item = Question.objects.all().get(pk=questionID)
            item.answered = item.answered == False #Essentially, toggles it
            item.save()

            return HttpResponse("OK")
        else:
            raise ObjectDoesNotExist
    except ObjectDoesNotExist:
        print "Attempt to answerQuestion failed: a single check failed"
        print request.POST
        return HttpResponse("NO")