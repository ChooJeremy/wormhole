from django.contrib import admin
from models import *

# Register your models here.
class LectureAdmin(admin.ModelAdmin):
	list_display = ('name', 'url',)

class QuestionAdmin(admin.ModelAdmin):
	list_display = ('question_text',)

class AudienceLoginAdmin(admin.ModelAdmin):
	list_display = ('username', 'email', 'lecture')

admin.site.register(Question, QuestionAdmin)
admin.site.register(Lecture, LectureAdmin)
admin.site.register(AudienceLogin, AudienceLoginAdmin)