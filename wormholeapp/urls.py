from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static
from wormholeapp.views import *

urlpatterns = patterns('',
    url(r'^$', userLogin),
    url(r'deletequestion$', deleteQuestion),
    url(r'answerquestion$', answerQuestion),
    url(r'adminlogin$', adminLogin),
    url(r'login$', processLogin),
    url(r'upvote$', doUpvote),
    url(r'downvote$', doDownvote),
    url(r'questionupdate$', questionUpdate),
    url(r'^(.+)$', lecture),
)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)